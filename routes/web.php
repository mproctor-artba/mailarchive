<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/upload', 'ListController@upload')->name('upload');
Route::post('/search', 'SearchController@search')->name('search');

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/exports', 'HomeController@exports')->name('exports');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/list/{id}', 'ListController@viewList')->name('view-list');

Route::post('/upload/clean', 'SearchController@cleanFile')->name('clean-file');