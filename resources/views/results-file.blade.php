@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <style>
        .table-inside td{
            padding-left:0;
            padding-right:0;
            border-top:0;
        }
        .nopad-left{
            padding-left:0 !important;
            padding-right:0 !important;
        }
        .table td{
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
<div class="container">
    @if(sizeof($safeEmails) > 0)
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-header">
                    No Records Found for These Emails
                </div>
                <div class="card-body">
                    <div class="inner-table">
                        <table id="safeEmailsTable">
                        <thead>
                            <tr>
                                @for($i = 0; $i <= 2; $i++)
                                    <th>{{ $headers[$i] }}</th>
                                @endfor
                            </tr>
                        </thead>
                            <tbody>
                                @foreach($safeEmails as $safe)
                                    <tr>
                                        <td>{{ $safe }}</td>
                                        <td>{{ $firstNames[$namerow] }}</td>
                                        <td>{{ $lastNames[$namerow] }}</td>
                                    </tr>
                                    @php $namerow++ @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <form method="POST" action="{{ route('clean-file') }}">
                        @csrf
                        <input type="hidden" name="filetoclean" value="{{ $file }}">
                        <p>Click below to redownload your spreadsheet but only containing emails that are not in the archive.</p>
                        <input type="submit" name="submit" value="Clean My List" class="btn btn-danger">
                    </form>
                </div>
            </div>
        </div>

    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card" style="margin-bottom: 10px;">
            <div class="card-body">
            <table class="table" id="notsafeEmailsTable">
                <thead>
                    <tr>
                        <th width="40%" style="padding-right: 0;">Contact</th>
                        <th width="252px" class="nopad-left">List Name</th>
                        <th width="152px" class="nopad-left">Status</th>
                        <th class="text-center nopad-left" width="152px" >Rating</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($emails as $email)
                    @php 
                            $lists = \App\Contact::where('email', $email)->get();
                                        @endphp
                                        @foreach($lists as $list)
                        <tr>
                            <td class="text-left"><strong>{{ $email }}</strong></td>
                            <td>{{ $list->list->name }}</td>
                            <td>{{ translateStatus($list->list->type) }}</td>
                            <td class="text-center">{{ $list->rating }}</td>
                        </tr>
                        @endforeach
                @endforeach 
                </tbody>
            </table>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.8/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    @if(sizeof($safeEmails) > 0)
    $("#safeEmailsTable").DataTable({
            responsive: true
        });
    @endif
    $("#notsafeEmailsTable").DataTable({
            responsive: true
        });
    </script>
@endsection
