@extends('layouts.app')

@section('content')
<div class="container">
<div class="row justify-content-center" style="margin-bottom: 10px;">
    <div class="col-md-8">
        @include('layouts.alerts')
    </div>
</div>
    <div class="row" style="margin-bottom: 10px;">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Search</div>
                <div class="card-body">
                    <form method="POST" action="{{ route('search') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="form-group">
                            <label>Separate emails by new lines, do not add commas</label>
                            <textarea class="form-control" name="emails" placeholder="email1 
email2
email3"></textarea>
                        </div>
                        <div class="form-group">
                            <h3>OR</h3>
                            <p>Upload a spreadsheet (CSV only) that meets the following criteria</p>
                            <ul>
                                <li>Column headers in the first row</li>
                                <li>Emails in the first column</li>
                                <li>First Names in the second Column</li>
                                <li>Last Names in the third Column</li>
                            </ul>
                            <input type="file" name="mylist" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="submit" name="submit" value="Search" class="btn btn-primary">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">Lists</div>
                <div class="card-body">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th class="text-center">Cleaned</th>
                                <th class="text-center">Unsubsubcribes</th>
                                <th class="text-center">Subscribers</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lists as $list)
                                @php
                                    $stats = $list->countStats();

                                @endphp
                                <tr>
                                    <td><a href="/list/{{ $list->id }}">{{ $list->name }}</a></td>
                                    <td class="text-center">{{ $stats["cleaned"] }}</td>
                                    <td class="text-center">{{ $stats["unsubscribed"] }}</td>
                                    <td class="text-center">{{ $stats["subscribers"] }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Upload New Emails</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <form method="POST" action="{{ route('upload') }}" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label>List Name</label>
                            <input type="text" name="name" class="form-control" required="" placeholder="i.e. Past Webinar Attendees">
                        </div>
                        <div class="form-group">
                            <label>Status</label>
                            <select name="type" class="form-control" required="">
                                <option>Select the current mailing status of these contacts</option>
                                <option value="1">Subscribers</option>
                                <option value="2">Cleaned</option>
                                <option value="3">Unsubscribers</option>
                            </select>
                            <p><small>This can sometimes be found in the file name. Example file name: <strong><i>subscribed_members</i></strong>_export_099ewewf.csv</small></p>
                        </div>
                        <div class="form-group">
                            <label>Upload Contacts (CSV only)</label>
                            <input type="file" class="form-control" name="emails" required="">
                            <p><small>For best results, do not make any modifications to the exported CSV files from mailchimp. Upload them as they are.</small></p>
                        </div>
                        <div class="form-group">
                            <input type="submit" class="btn btn-success" name="submit" value="Upload">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
