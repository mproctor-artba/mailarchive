@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <style>

        .table td{
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-header">
                    <p>{{ $lists->first()->name }}</p>
                </div>
                <div class="card-body">
                    <table id="safeEmailsTable" class="table">
                        <thead>
                            <tr>
                                <th>Contact</th>
                                <th class="text-center">Status</th>
                                <th class="text-center">Rating</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($lists as $list)
                                @php $contacts = \App\Contact::where('list_id', $list->id)->get(); @endphp
                                    @foreach($contacts as $contact)
                                    <tr>    
                                        <td class="text-left"><strong>{{ $contact->email }}</strong></td>
                                        <td class="text-center">{{ translateStatus($list->type) }}</td>
                                        <td class="text-center">{{ $contact->rating }}</td>
                                    </tr>
                                    @endforeach
                            @endforeach 
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $("#safeEmailsTable").DataTable();
    </script>
@endsection
