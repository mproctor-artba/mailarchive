@extends('layouts.app')

@section('css')
    <style>
        .table-inside td{
            padding-left:0;
            padding-right:0;
            border-top:0;
        }
        .nopad-left{
            padding-left:0 !important;
            padding-right:0 !important;
        }
        .table td{
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
<div class="container">
<div class="row justify-content-center" style="margin-bottom: 10px;">
    <div class="col-md-8">
        @include('layouts.alerts')
    </div>
</div>
    @if(sizeof($safeEmails) > 0)
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-header">
                    No Records Found for These Emails
                </div>
                <div class="card-body">
                    <ul>
                         @foreach($safeEmails as $safe)
                            <li>{{ $safe }}</li>
                         @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
    @endif
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card" style="margin-bottom: 10px;">
            <table class="table">
                <thead>
                    <tr>
                        <th width="40%" style="padding-right: 0;">Contact</th>
                        <th width="252px" class="nopad-left">List Name</th>
                        <th width="152px" class="nopad-left">Status</th>
                        <th class="text-center nopad-left" width="152px" >Rating</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($emails as $email)
                            <tr>
                                <td class="text-left"><strong>{{ $email }}</strong></td>
                                <td colspan="3" class="nopad-left">
                                    <table width="100%" class="table-inside">
                                        <tbody>
                                            @php 
                                                $lists = \App\Contact::where('email', $email)->get();
                                            @endphp
                                            @foreach ($lists as $list)
                                                <tr>
                                                    <td class="text-left" width="252px">{{ $list->list->name }}</td>
                                                    <td width="152px">{{ translateStatus($list->list->type) }}</td>
                                                    <td class="text-center" width="152px">{{ $list->rating }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                    @endforeach 
                    </tbody>
            </table>
            </div>
        </div>
    </div>
</div>
@endsection
