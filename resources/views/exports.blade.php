@extends('layouts.app')

@section('css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>

    <style>

        .table td{
            vertical-align: middle;
        }
    </style>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card" style="margin-bottom: 10px;">
                <div class="card-header">
                    <p>My Cleaned Imports</p>
                </div>
                <div class="card-body">
                    <table id="safeEmailsTable" class="table">
                        <tbody>
                            @foreach($downloads as $download)
                                <tr>    
                                    <td class="text-left"><strong><a href="/uploads/{{ $download->file }}">{{ $download->file }}</a></strong></td>
                                    <td class="text-center">{{ $download->created_at }}</td>
                                </tr>
                            @endforeach 
                            </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript">
    $("#safeEmailsTable").DataTable();
    </script>
@endsection
