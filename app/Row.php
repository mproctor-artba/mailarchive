<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Row extends Model
{
    public function column(){
    	return $this->belongsTo('\App\Column');
    }
}
