<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingList extends Model
{
	protected $table = "lists";

	public function user()
	{
		return $this->belongsTo('\App\User');
	}

    public function columns()
    {
        return $this->hasMany('\App\Column', 'list_id');
    }

    public function scopeCountStats(){
    	$stats = ["cleaned" => 0, "unsubscribed" => 0, "subscribers" => 0];

    	for($i = 1; $i <= 3; $i++){
    		$list = \App\MailingList::where('name', $this->name)->where('type', $i)->first();
    		if(isset($list)){
    			switch ($i) {
    				case $i == 1:
    					$stats["subscribers"] = \App\Contact::where('list_id', $list->id)->count();
    					break;
    				case $i == 2:
    					$stats["cleaned"] = \App\Contact::where('list_id', $list->id)->count();
    					break;
    				case $i == 3:
    					$stats["unsubscribed"] = \App\Contact::where('list_id', $list->id)->count();
    					break;
    				default:
    					break;
    			}
    		}
    	}

    	return $stats;
    }
}
