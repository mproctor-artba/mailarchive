<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Column extends Model
{
    public function rows()
    {
        return $this->hasMany('\App\Row');
    }

    public function list(){
    	return $this->belongsTo('\App\MailingList');
    }

    public function rank(){
    	return App\MailingList::find($this->list_id);
    }
}
