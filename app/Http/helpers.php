<?php



function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomNumString($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function translateStatus($i){
    switch ($i) {
        case $i == 1:
            return "Subscribed";
        case $i == 2:
            return "Cleaned";
        case $i == 3:
            return "Unsubscribed";
            break;
        default:
            # code...
            break;
    }
}

function bsAlert($alert, $class){
    $alertID = rand(1111,9999);
    echo '<div class="alert alert-' . $class . ' alert-styled-left" id="alert1">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            ' . $alert . '
        </div>
        <script>setTimeout("alertHide(1)", 3000); </script>
        ';
}