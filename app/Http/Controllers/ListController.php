<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MailingList as MailingLists;
use App\Column as Columns;
use App\Row as Rows;
use Auth;
use DB;
use Session;
use Redirect;

class ListController extends Controller
{
    public function upload(Request $request){
        set_time_limit(300);
    	$destination = public_path() . "/uploads/";
         if($request->file('emails') !== null){
            $emails = $request->file('emails');
            $fileName = generateRandomString() . "-" . $emails->getClientOriginalName();
            $emails->move($destination, $fileName); 

            // create the list
            DB::table('lists')->insert([
                "user_id" => Auth::user()->id,
                "name" => $request->name,
                "type" => $request->type,
                "source" => "mc"
            ]);

            $listID = DB::getPdo()->lastInsertId();

            $rows = [];
            $row = 0;
            $imported = 0;
            $headers = [];
            $emailColumn = 0;
            $ratingColumn = 0;

            if (($handle = fopen(public_path() . "/uploads/$fileName", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                	// get row data from within the CSV file
                	$num = count($data);
                    $rows[$row] = array();
                    for ($c=0; $c < $num; $c++) {
                        array_push($rows[$row], $data[$c]);
                        if($row == 0){
                            // headers
                            /* exclude certain columns because they will get stored in separate table */
                            
                           

                            if($data[$c] == "Email Address" || $data[$c] == "email"){
                                $emailColumn = $c;
                            }
                            if($data[$c] == "MEMBER_RATING"){
                                $ratingColumn = $c;
                            }

                            DB::table('columns')->insert([
                                "list_id" => $listID,
                                "name" => $data[$c]
                            ]);
                            $headers{$c} = DB::getPdo()->lastInsertId();
                               
                        } else {
                            // table content
                            if(empty($data[$c])){
                                $data[$c] = NULL;
                            }
                            if(isset($headers[$c]) && $emailColumn != $c && $ratingColumn != $c){
                                DB::table('rows')->insert([
                                    'column_id' => $headers[$c],
                                    'content' => $data[$c]
                                ]);
                            }
                            if($emailColumn == $c){
                                DB::table('contacts')->insert([
                                    "user_id" => Auth::user()->id,
                                    "email" => $data[$c],
                                    "list_id" => $listID
                                    
                                ]);

                                $contactID = DB::getPdo()->lastInsertId();
                            }
                            if($ratingColumn == $c && isset($contactID)){
                                DB::table('contacts')->where('id', $contactID)->update([
                                    'rating' => $data[$c]
                                ]);
                            }
                        }
                    }
                    $row++;
                }
            }
        }
        $num = sizeof($rows) - 1;
        Session::flash('success', "Successfully archived $num " . translateStatus($request->type) . " Contacts into the $request->name List");

        return Redirect::to('home');
    }

    public function viewList($id){
        $list = MailingLists::find($id);
        $lists = MailingLists::where('name', $list->name)->get();

        $data = [
            "lists" => $lists
        ];

        return view('list', $data);

    }
}
