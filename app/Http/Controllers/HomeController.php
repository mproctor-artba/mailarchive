<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MailingList as Lists;
use App\Download as Downloads;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $lists = Lists::groupBy('name')->select(array('name','id'), DB::raw('count(*) as total'))->get();




        $data = [
            "lists" => $lists
        ];

        return view('home', $data);
    }

    public function exports(){
        $data = [
            "downloads" => Auth::user()->downloads
        ];

        return view('exports', $data);
    }
}
