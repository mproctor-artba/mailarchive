<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MailingList as MailingLists;
use App\Row as Rows;
use App\Contact as Contacts;
use App\Download as Downloads;
use Auth;
use DB;
use Session;
use Redirect;

class SearchController extends Controller
{
	public function search(Request $request){

		$safe = [];
		$existingContacts = [];
		if($request->file('mylist') === null){
			$emails = explode("\n", $request->emails);

			foreach ($emails as $email) {
				$email = str_replace("\r", "", $email);
				$contacts = Contacts::where('email', $email)->get();
				if($contacts->count() == 0){
					array_push($safe, $email);
				} else {
					array_push($existingContacts, $email);
				}
			}

			$data = [
				"emails" => $existingContacts,
				"safeEmails" => $safe
			];

			return view('results', $data);
		}
		else {
			// user uploaded a file to be used for search

			set_time_limit(300);
	    	$destination = public_path() . "/uploads/";

	    	$emails = $request->file('mylist');
            $fileName = generateRandomString() . "-" . $emails->getClientOriginalName();
            $emails->move($destination, $fileName); 

            $emails = [];
            $rows = [];
            $row = 0;
            $imported = 0;
            $headers = [];
            $firstNames = [];
            $lastNames = [];

            if (($handle = fopen(public_path() . "/uploads/$fileName", "r")) !== FALSE) {
                while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                	// get row data from within the CSV file
                	$num = count($data);
                    $rows[$row] = array();
                    for ($c=0; $c < $num; $c++) {

                    	if($row == 0){
                            // headers
                            /* exclude certain columns because they will get stored in separate table */
                            array_push($headers, $data[$c]);
                               
                        } else {
                            // table content
                            if(empty($data[$c])){
                                $data[$c] = NULL;
                            }
                            if($c == 0){ 
                            	array_push($emails, $data[$c]);
                            }
                            if($c == 1){
                            	array_push($firstNames, $data[$c]);
                            }
                            if($c == 2){
                            	array_push($lastNames, $data[$c]);
                            }
                            
                        }
                    }
                    $row++;
                }
            }

            foreach ($emails as $email) {
				$email = str_replace("\r", "", $email);
				$contacts = Contacts::where('email', $email)->get();
				if($contacts->count() == 0){
					array_push($safe, $email);
				} else {
					array_push($existingContacts, $email);
				}
			}

			$data = [
				"emails" => $existingContacts,
				"safeEmails" => $safe,
				"file" => $fileName,
				"headers" => $headers,
				"firstNames" => $firstNames,
				"lastNames" => $lastNames,
				"namerow" => 0
			];

			return view('results-file', $data);

		}
	}

	public function cleanFile(Request $request){
		set_time_limit(300);
		$file = $request->filetoclean;
	   	$cleanrow = [];
	   	$rows = [];
        $row = 0;
        $headers = [];

		if (($handle = fopen(public_path() . "/uploads/$file", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
            	$num = count($data);
                $rows[$row] = array();
                for ($c=0; $c < $num; $c++) {
                	if($row == 0){
                        // capture file headers
                        array_push($headers, $data[$c]);
                    } else {
                        // table content
                        
                        if($c == 0){ 
                        	// check to see if email is unique
                        	if(Contacts::where('email',$data[$c])->where('user_id', Auth::user()->id)->count() == 0){
                        		// email is clean... push it into the clean row array
                        		$cleanrow{$row} = $data;
                        	}
                        }
                    }
                }
                $row++;
            }
        }

        // create a new csv file

        $cleanedfile = $this->generateExport($headers, $cleanrow);

        DB::table('downloads')->insert([
        	'user_id' => Auth::user()->id,
        	'file' => $cleanedfile
        ]);

        Session::flash('success', "Your cleaned list is ready to download!");

        return Redirect::route('exports');

	}

	public function generateExport($headers, $rows){

		$filelocation = public_path() . "/uploads/";
		$filename = 'ListExport-'.date('Y-m-d H.i.s').'.csv';
        $file_export  =  $filelocation . $filename;

        // CSV column names

        $csv_fields = array();

        foreach($headers as $header){
        	$csv_fields[] = $header;
        }

        $data = fopen($file_export, 'w');
        fwrite($data, implode(",", $csv_fields));

        foreach($rows as $row){
        	$csv_field = $row;
        	fwrite($data, "\n" . implode(",", $csv_field));
        }

        
        unset($csv_field);

        return $filename;
        
	}
}